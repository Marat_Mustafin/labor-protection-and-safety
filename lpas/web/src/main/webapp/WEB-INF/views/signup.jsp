<%@ page contentType="text/html" language="java" pageEncoding="UTF-8" %>
<%@ include file="part/header.jsp" %>

<center>
    <%if (null != request.getParameter("alert")) {%>
    <h2><font color="red"> Пользователь с именем <%=request.getParameter("alert")%> уже существует</font></h2>
	<% } else if (null != request.getParameter("succes")) {%>
    <h2><font color="green"> Пользователь с именем <%=request.getParameter("succes")%> удачно создан</font></h2>
	<%}%>
    <p>Пожалуйста, создайте новую учётную запись</p>
    <form action="do?command=SignUp" method="post">
        <table border="0">
            <tr><td><input name="name" type="text" placeholder="Имя" required="" value=""></td></tr>
            <tr><td><input  type="submit" value="Зарегестрировать"></td></tr>
        </table>
    </form>
</center>

<%@ include file="part/footer.jsp" %>
