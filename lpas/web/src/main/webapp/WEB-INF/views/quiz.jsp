<%-- 
    Document   : selectTheme
    Created on : 25.12.2017, 17:09:04
    Author     : Me
--%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.ctc34.lpas.controller.Messages"%>
<%@page import="com.ctc34.lpas.controller.Content"%>
<%@page import="bean.User"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.Random"%>
<%@page import="java.util.Collections"%>
<%@page import="bean.Ticket"%>
<%@page import="java.util.List"%>
<%@page import="bean.Chapter"%>
<%@page contentType="text/html" language="java" pageEncoding="UTF-8" %>
<%@include file="part/header.jsp" %>

<%Ticket ticket = Content.getTicket(request);
    List<String> attrs = new ArrayList<>();
    if (session.getAttribute(Messages.ANSWER_ATTRS) != null) {
	attrs = (List<String>) session.getAttribute(Messages.ANSWER_ATTRS);
    } else {
	attrs = Arrays.asList(new String[]{"id=\"neutral\"", "id=\"neutral\"", "id=\"neutral\"", "id=\"neutral\""});
    }
%>
<center>
    <form action="do?command=check" method="POST">
	<table border="0" cellpadding="1">
	    <thead> 
		<tr>
		    <th colspan="2"><%= ticket.getQuestion()%></th>
		</tr>
	    </thead>
	    <tbody>
		<tr>
		    <td><button <%=attrs.get(0)%> name="select" value="0"><%= ticket.getAnswers()[0]%></button></td>
		</tr>
		<tr>
		    <td><button <%=attrs.get(1)%> name="select" value="1"><%= ticket.getAnswers()[1]%></button></td>
		</tr>
		<tr>
		    <td><button <%=attrs.get(2)%> name="select" value="2"><%= ticket.getAnswers()[2]%></button></td>
		</tr>
		<tr>
		    <td><button <%=attrs.get(3)%> name="select" value="3"><%= ticket.getAnswers()[3]%></button></td>
		</tr>
	    </tbody>
	</table>
	<%if (session.getAttribute(Messages.ANSWER_ATTRS) != null) {
		out.append(attrs.get(4));
	    }%>
    </form>
</center>
<% session.removeAttribute(Messages.ANSWER_ATTRS);%>
<%@ include file="part/footer.jsp" %>