<%@page import="bean.User"%>
<%@page import="java.util.List"%>
<%@page import="dao.DAO"%>
<%@ page contentType="text/html" language="java" pageEncoding="UTF-8" %>
<%@ include file="part/header.jsp" %>
<% if (null != request.getParameter("done")) {
	out.append("<h2><font color=\"green\">Запись обновлена</font></h2>");
    }%>
<form action="do?command=Statistics" method="post">
    <table border="1" cellpadding="2">
	<% List<User> users = DAO.getDAO().user.getAll(" WHERE admin='0' AND NOT correct='0'");
	    if (users.size() > 0) {
		out.append("<thead><tr><th>Имя пользователя</th><th>Правильных ответов</th><th>Удалить</th><th>Выбрать</th></tr></thead><tbody>");
		for (User user : users) {
		    out.append("<tr>"
			    + "<td>" + user.getName() + "</td>"
			    + "<td>" + user.getCorrect() + "</td>"
			    + "<td><button formaction=\"do?command=Statistics&delete=" + user.getId_user() + "\">Удалить</button></td>"
			    + "<td><input type=\"checkbox\" name=\"delete\" value=\"" + user.getId_user() + "\"></td>"
			    + "</tr><tr>"
			    + "</tr>");
		}
		out.append("<td colspan=\"4\"><input type=\"submit\" value=\"Удалить выбранные\"></td></tbody>");
	    } else {
		out.append("<h3><font color=\"red\">Таблица статистики пуста</font></h3>");
	    }
	%>
    </table>
</form>

<%@ include file="part/footer.jsp" %>