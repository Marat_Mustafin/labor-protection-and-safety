/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctc34.lpas.formHandler;

import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author marat
 */
public class PostFactory {

    public CommandHandler getCommnad(HttpServletRequest req) {
	switch (req.getParameter("command").toLowerCase()) {
	    case "signup":
		return new HandlerSignup();
	    case "login":
		return new HandlerLogIn();
	    case "check":
		return new HandlerCheck();
	    case "statistics":
		return new HandlerStatistics();
	    case "final":
		return new HandlerFinal();
	    default:
		return null;
	}
    }
}
