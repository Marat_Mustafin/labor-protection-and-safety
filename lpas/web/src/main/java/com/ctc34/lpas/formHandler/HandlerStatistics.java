/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctc34.lpas.formHandler;

import bean.User;
import dao.DAO;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author marat
 */
public class HandlerStatistics implements CommandHandler {

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) {
	try {
	    DAO dao = DAO.getDAO();
	    String delVal[] = req.getParameterValues("delete");
	    for (String id : delVal) {
		User user = dao.user.read(Integer.parseInt(id));
		user.setCorrect(0);
		dao.user.update(user);
	    }
	    resp.sendRedirect("do?command=Statistics");
	} catch (SQLException | IOException ex) {
	    Logger.getLogger(HandlerStatistics.class.getName()).log(Level.SEVERE, null, ex);
	}
    }

}
