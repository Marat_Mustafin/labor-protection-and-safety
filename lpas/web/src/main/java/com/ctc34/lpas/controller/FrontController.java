package com.ctc34.lpas.controller;

import controller.formHandler.CommandHandler;
import controller.formHandler.PostFactory;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class FrontController extends HttpServlet {

    private GetFactory getFactory = new GetFactory();
    private PostFactory postHandler = new PostFactory();

    @Override
    public void init() throws ServletException {
	super.init();
	//getServletContext().setAttribute("chapters", Content.getChapters());//TODO
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	getFactory.getPage(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	CommandHandler commandHandler = postHandler.getCommnad(req);
	commandHandler.execute(req, resp);
    }

}
