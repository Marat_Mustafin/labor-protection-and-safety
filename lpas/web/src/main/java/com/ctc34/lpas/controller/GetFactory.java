/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author marat
 */
public class GetFactory {

    void getPage(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	switch (req.getParameter("command")) {
	    case "SignUp":
		req.getRequestDispatcher("/signup.jsp").forward(req, resp);
		break;
	    case "LogIn":
		req.getRequestDispatcher("/login.jsp").forward(req, resp);
		break;
	    case "Quiz":
		req.getRequestDispatcher("/quiz.jsp").forward(req, resp);
		break;
	    case "Check":
		req.getRequestDispatcher("/check.jsp").forward(req, resp);
		break;
	    case "Statistics":
		req.getRequestDispatcher("/statistics.jsp").forward(req, resp);
		break;
	    case "Final":
		req.getRequestDispatcher("/final.jsp").forward(req, resp);
		break;
	    case "LogOut":
		logOut(req, resp);
		break;
	    default:
		req.getRequestDispatcher("/index.jsp").forward(req, resp);
		break;
	}
    }

    private void logOut(HttpServletRequest req, HttpServletResponse resp) {
	HttpSession session = req.getSession(false);
	if (null != session) {
	    session.invalidate();
	    try {
		resp.sendRedirect("/osah/");
	    } catch (IOException ex) {
		Logger.getLogger(GetFactory.class.getName()).log(Level.SEVERE, null, ex);
	    }
	}
    }
}
