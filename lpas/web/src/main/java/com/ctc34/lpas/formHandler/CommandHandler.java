/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctc34.lpas.formHandler;//TODO

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author marat
 */
public interface CommandHandler {

    void execute(HttpServletRequest req, HttpServletResponse resp);
}
