/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctc34.lpas.formHandler;

import com.ctc34.lpas.controller.Messages;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author me
 */
public class HandlerCheck implements CommandHandler {

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) {
	HttpSession session = req.getSession(false);
	int select = Integer.parseInt(req.getParameter(Messages.SELECT));
	int correct = (int) session.getAttribute(Messages.CORRECT);
	if (session.getAttribute(Messages.SCORE) == null) {
	    session.setAttribute(Messages.SCORE, 0);
	}
	int score = (int) session.getAttribute(Messages.SCORE);
	if (select == correct) {
	    session.setAttribute(Messages.SCORE, ++score);
	}
	try {
	    if (((Integer) session.getAttribute(Messages.POSITION) + 1) == (Integer) req.getServletContext().getAttribute(Messages.TOTAL)) {
		    resp.sendRedirect("do?command=Final");
		} else {
		    boolean show = (boolean) session.getAttribute(Messages.SHOW);
		    session.setAttribute(Messages.SHOW, !show);
		    resp.sendRedirect("do?command=Quiz");
		}
	} catch (IOException ex) {
	    ex.printStackTrace();
	}
    }

}
