package com.ctc34.lpas.formHandler;

import bean.User;
import com.ctc34.lpas.controller.Messages;
import com.ctc34.lpas.;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class HandlerLogIn implements CommandHandler {

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) {
	DAO dao = DAO.getDAO();
	try {
	    List<User> users = dao.user.getAll(" WHERE name='" + req.getParameter("name") + "'");
	    req.setAttribute(Messages.CURRENT, "LogIn");
	    if (users.size() == 1) {
		HttpSession session = req.getSession(true);
		session.setAttribute("user", users.get(0));
		req.setAttribute(Messages.MESSAGE, "finish");
		resp.sendRedirect("/osah/");
	    } else {
		req.setAttribute(Messages.ERROR, "not found");
		req.getRequestDispatcher("login.jsp").forward(req, resp);
	    }
	} catch (ServletException | IOException ex) {
	    Logger.getLogger(HandlerLogIn.class.getName()).log(Level.SEVERE, null, ex);
	}
    }

}
