package controller.formHandler;

import bean.User;
import controller.Messages;
import dao.DAO;
import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HandlerSignup implements CommandHandler {

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) {
	try {
	    req.setAttribute(Messages.CURRENT, "SignUp");
	    DAO dao = DAO.getDAO();
	    if (dao.user.getAll(" WHERE name='" + req.getParameter("name") + "'").size() > 0) {
		req.setAttribute(Messages.ERROR, "user already exists");
		resp.sendRedirect("do?command=SignUp&alert=" + req.getParameter("name"));
	    } else {
		User user = new User(req.getParameter("name"), 0);
		dao.user.create(user);
		resp.sendRedirect("/osah/do?command=SignUp&succes=" + req.getParameter("name"));
	    }
	} catch (SQLException | IOException e) {
	    req.getServletContext().log(e.getMessage());
	    req.setAttribute(Messages.ERROR, e.getMessage());
	}
    }
}
