/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctc34.lpas.formHandler;

import bean.User;
import com.ctc34.lpas.controller.Messages;
import dao.DAO;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author me
 */
public class HandlerFinal implements CommandHandler {

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) {
	HttpSession session = req.getSession(false);
	User user = (User) session.getAttribute("user");
	if (null != user) {
	    user.setCorrect((Integer) session.getAttribute(Messages.SCORE));
	    try {
		DAO.getDAO().user.update(user);
		Enumeration attrs = session.getAttributeNames();
		while (attrs.hasMoreElements()) {
		    String attr = (String) attrs.nextElement();
		    if (attr.equals("user")) {
			continue;
		    }
		    session.removeAttribute(attr);
		}
	    } catch (SQLException ex) {
		ex.printStackTrace();
	    }
	}
	try {
            session.setAttribute(Messages.POSITION, -1);
	    resp.sendRedirect("/osah/");
	} catch (IOException ex) {
	    Logger.getLogger(HandlerFinal.class.getName()).log(Level.SEVERE, null, ex);
	}
    }

}
