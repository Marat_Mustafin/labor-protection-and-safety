/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctc34.lpas.prepareticket;

import dao.DAO;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author marat
 */
public class ContentCombiner {

    public static Ticket getTicket(HttpServletRequest req) {
	HttpSession session = req.getSession(false);
	setPack(req);
	if (session.getAttribute(Messages.TRYOUT) == null) {
	    session.setAttribute(Messages.TRYOUT, req.getParameter("samples"));
	}
	if (session.getAttribute(Messages.SHOW) == null) {
	    session.setAttribute(Messages.SHOW, false);
	}
	boolean show = (boolean) session.getAttribute(Messages.SHOW);
	if (show) {
	    session.setAttribute(Messages.ANSWER_ATTRS, getAnswerAttrs(session));
	    return (Ticket) session.getAttribute(Messages.TICKET);
	} else {
	    int position = (null != session.getAttribute(Messages.POSITION)) ? (int) session.getAttribute(Messages.POSITION) : -1;
	    Map<Integer, Ticket> pack = (Map<Integer, Ticket>) session.getAttribute(Messages.PACK);
	    Ticket ticket = pack.get(++position);
	    session.setAttribute(Messages.TICKET, ticket);
	    session.setAttribute(Messages.CORRECT, shuffleAnswers(ticket));
	    session.setAttribute(Messages.POSITION, position);
	    return ticket;
	}
    }
    
    public static Map<Chapter, List<Ticket>> getChapters() {
	Map<Chapter, List<Ticket>> chapters = new HashMap<>();
	DAO dao = DAO.getDAO();
	try {
	    List<Chapter> keys = dao.chapter.getAll();
	    for (Chapter chapter : keys) {
		chapters.put(chapter, dao.ticket.getAll(" WHERE fk_chapter='" + chapter.getId_chapter() + "'"));
	    }
	} catch (SQLException ex) {
	    ex.printStackTrace();
	}
	return chapters;
    }

    private static void setPack(HttpServletRequest req) {
	HttpSession session = req.getSession(false);
	if (null == session.getAttribute("pack")) {
	    synchronized (HashMap.class) {
		if (null == session.getAttribute("pack")) {
		    Map<Integer, Ticket> pack = new HashMap<>();
		    Map<Chapter, List<Ticket>> chapters = ((Map<Chapter, List<Ticket>>) req.getServletContext().getAttribute("chapters"));
		    int position = 0;
		    for (Map.Entry<Chapter, List<Ticket>> entry : chapters.entrySet()) {
			List<Ticket> tickets = entry.getValue();
			int random = ThreadLocalRandom.current().nextInt(0, tickets.size());
			Ticket ticket = tickets.get(random);
			pack.put(position++, ticket);
		    }
		    List<Chapter> keys = new ArrayList<>(chapters.keySet());
		    Collections.shuffle(keys, new Random(System.nanoTime()));
		    int count = 4;
		    http://begin.of.iteration
		    for (Chapter ch : keys) {
			while (0 < count) {
			    List<Ticket> additionalListTk = chapters.get(ch);
			    int random = ThreadLocalRandom.current().nextInt(0, additionalListTk.size());
			    Ticket additionalTk = additionalListTk.get(random);
			    for (Map.Entry<Integer, Ticket> entry : pack.entrySet()) {
				if (additionalTk.getId_ticket() != entry.getValue().getId_ticket()) {
				    pack.put(position++, additionalTk);
				    count--;
				    continue http;
				}
			    }
			}
		    }
		    List<Integer> keys1 = new ArrayList<>(pack.keySet());
		    Collections.shuffle(keys, new Random(System.nanoTime()));
		    Map<Integer, Ticket> chaptersShuffled = new HashMap<>(pack.size());
		    for (Integer ch : keys1) {
			chaptersShuffled.put(ch, pack.get(ch));
		    }
		    ServletContext context = req.getServletContext();
		    if (null == context.getAttribute(Messages.TOTAL)) {
			context.setAttribute(Messages.TOTAL, pack.size());
		    }
		    session.setAttribute("pack", chaptersShuffled);
		}
	    }
	}
    }

    private static int shuffleAnswers(Ticket ticket) {
	String answers[] = ticket.getAnswers();
	String correct = answers[0];
	List<String> answersList = Arrays.asList(ticket.getAnswers());
	Collections.shuffle(answersList, new Random(System.nanoTime()));
	int it = -1;
	for (String string : answersList) {
	    it++;
	    if (string.equals(correct)) {
		break;
	    }
	}
	ticket.setAnswers(answersList.toArray(answers));
	return it;
    }

    private static List<String> getAnswerAttrs(HttpSession session) {
	List<String> attrs = new ArrayList<>();
	int correct = (int) session.getAttribute(Messages.CORRECT);
	for (int i = 0; i < 4; i++) {
	    if (correct == i) {
		attrs.add("id=\"correct\" disabled=\"\"");
		continue;
	    }
	    attrs.add("id=\"wrong\" disabled=\"\"");
	}
	attrs.add("<button formaction=\"do?command=check\" id=\"neutral\" name=\"select\" value=\"4\">Далее</button>");
	return attrs;
    }

}
