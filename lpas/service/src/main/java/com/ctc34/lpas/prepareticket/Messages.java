/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctc34.lpas.prepareticket;

/**
 *
 * @author Admin
 */
public interface Messages {

    String MESSAGE = "message";
    String ERROR = "error";
    String CURRENT = "current";
    String POSITION = "position";
    String CORRECT = "correct";
    String SCORE = "score";
    String SELECT = "select";
    String TOTAL = "total";
    String TRYOUT = "tryout";
    String ANSWERS = "answers";
    String TICKET = "ticket";
    String PACK = "pack";
    String SHOW = "show";
    String ANSWER_ATTRS = "answerAttrs";
}
