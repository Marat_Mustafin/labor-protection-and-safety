/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctc34.lpas.repository;

import com.ctc34.lpas.connection.Pool;
import com.ctc34.lpas.domain.Chapter;
import com.ctc34.lpas.domain.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author Admin
 */
public class ChapterRepository extends AbstractRepository<Chapter> {

    public ChapterRepository() {
        super.DELETE_SQL = "DELETE FROM chapters WHERE id_chapter=?";
    }

    @Override
    public Optional<Chapter> readById(int id) {
        Chapter chapter = null;
        try (Connection connection = Pool.INSTANSE.getConnection();
                PreparedStatement preparedStatement = createPreparedStatementRead(connection, id);
                ResultSet resultSet = preparedStatement.executeQuery();) {
            while (resultSet.next()) {
                chapter = new Chapter();
                chapter.setId(resultSet.getInt("id_chapter"));
                chapter.setName(resultSet.getString("name"));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return Optional.ofNullable(chapter);
    }
    
    private PreparedStatement createPreparedStatementRead(Connection connection, int id) throws SQLException {//TODO revert back old style as it was
        String SQL_QUERY = "SELECT id_chapter, name FROM chapters WHERE id_chapter=?";
        PreparedStatement preparedStatement = connection.prepareStatement(SQL_QUERY);
        preparedStatement.setInt(1, id);
        return preparedStatement;
    }

    @Override
    public List<Chapter> readAll() {
        List<Chapter> chapters = new ArrayList<>();
        String SQL_QUERY = "SELECT id_chapter, name FROM chapters";
        try (Connection connection = Pool.INSTANSE.getConnection();
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(SQL_QUERY);) {
            while (resultSet.next()) {
                Chapter chapter = new Chapter();
                chapter.setId(resultSet.getInt("id_chapter"));
                chapter.setName(resultSet.getString("name"));
                chapters.add(chapter);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return chapters;
    }

    @Override
    public int create(Chapter chapter) {
        int createdId = 0;
        String SQL_QUERY = "INSERT INTO chapters (name) VALUES (?)";
        ResultSet resultSet = null;
        try (Connection connection = Pool.INSTANSE.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SQL_QUERY, Statement.RETURN_GENERATED_KEYS);) {
            preparedStatement.setString(1, chapter.getName());
            preparedStatement.executeUpdate();
            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                createdId = resultSet.getInt(1);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (null != resultSet) {
                try {
                    resultSet.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return createdId;
    }

    @Override
    public boolean update(Chapter chapter) {
        String SQL_QUERY = "UPDATE chapters SET name=? WHERE id_chapter=?";
        boolean success = false;
        try (Connection connection = Pool.INSTANSE.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SQL_QUERY);) {
            preparedStatement.setString(1, chapter.getName());
            preparedStatement.setInt(2, chapter.getId());
            success = (preparedStatement.executeUpdate() == 1);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return success;
    }

}
