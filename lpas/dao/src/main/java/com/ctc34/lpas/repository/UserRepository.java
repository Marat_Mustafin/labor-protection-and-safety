/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctc34.lpas.repository;

import com.ctc34.lpas.connection.Pool;
import com.ctc34.lpas.domain.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author Admin
 */
public class UserRepository extends AbstractRepository<User> {

    public UserRepository() {
        super.DELETE_SQL = "DELETE FROM users WHERE id_user=?";
    }

    @Override
    public Optional<User> readById(int id) {
        User user = null;
        try (Connection connection = Pool.INSTANSE.getConnection();
                PreparedStatement preparedStatement = createPreparedStatementRead(connection, id);
                ResultSet resultSet = preparedStatement.executeQuery();) {
            while (resultSet.next()) {
                user = new User();
                user.setId(resultSet.getInt("id_user"));
                user.setLogin(resultSet.getString("login"));
                user.setPassword(resultSet.getString("password"));
                user.setEmail(resultSet.getString("email"));
                user.setRole(User.Role.values()[resultSet.getInt("fk_role")]);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return Optional.ofNullable(user);
    }

    private PreparedStatement createPreparedStatementRead(Connection connection, int id) throws SQLException {//TODO revert back old style as it was
        String SQL_QUERY = "SELECT id_user, login, password, email,fk_role FROM users WHERE id_user=?";
        PreparedStatement preparedStatement = connection.prepareStatement(SQL_QUERY);
        preparedStatement.setInt(1, id);
        return preparedStatement;
    }

    @Override
    public List<User> readAll() {
        List<User> users = new ArrayList<>();
        String SQL_QUERY = "SELECT id_user, login, password, email, fk_role FROM users";
        try (Connection connection = Pool.INSTANSE.getConnection();
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(SQL_QUERY);) {
            while (resultSet.next()) {
                User user = new User();
                user.setId(resultSet.getInt("id_user"));
                user.setLogin(resultSet.getString("login"));
                user.setPassword(resultSet.getString("password"));
                user.setEmail(resultSet.getString("email"));
                user.setRole(User.Role.values()[resultSet.getInt("fk_role")]);
                users.add(user);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return users;
    }

    @Override
    public int create(User user) {
        int createdId = 0;
        String SQL_QUERY = "INSERT INTO users (login, password, email, fk_role) VALUES (?,?,?,?)";
        ResultSet resultSet = null;
        try (Connection connection = Pool.INSTANSE.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SQL_QUERY, Statement.RETURN_GENERATED_KEYS);) {
            preparedStatement.setString(1, user.getLogin());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.setString(3, user.getEmail());
            preparedStatement.setInt(4, user.getRole().ordinal());
            preparedStatement.executeUpdate();
            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                createdId = resultSet.getInt(1);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (null != resultSet) {
                try {
                    resultSet.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return createdId;
    }

    @Override
    public boolean update(User user) {
        String SQL_QUERY = "UPDATE users SET login=?, password=?, email=?, fk_role=? WHERE id_user=?";
        boolean success = false;
        try (Connection connection = Pool.INSTANSE.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SQL_QUERY);) {
            preparedStatement.setString(1, user.getLogin());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.setString(3, user.getEmail());
            preparedStatement.setInt(4, user.getRole().ordinal());
            preparedStatement.setInt(5, user.getId());
            success = (preparedStatement.executeUpdate() == 1);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return success;
    }
}
