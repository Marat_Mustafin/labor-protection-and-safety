/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctc34.lpas.connection;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import javax.sql.DataSource;

/**
 *
 * @author Admin
 */
public enum Pool {

    INSTANSE;

    private DataSource dataSource = null;

    public static Pool getINSTANSE() {
        return INSTANSE;
    }

    public Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }

    private Pool() {
        dataSource = createDataSource();
    }

    private File getFileFromResources() {
        ClassLoader classLoader = getClass().getClassLoader();
        URL resource = classLoader.getResource("datasource.properties");
        if (null == resource) {
            throw new RuntimeException("datasourse.properties not found");
        } else {
            return new File(resource.getFile());
        }
    }

    private Properties readProperties() {
        Properties properties = new Properties();
        try (InputStream input = new FileInputStream(getFileFromResources())) {
            properties.load(input);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return properties;
    }

    private DataSource createDataSource() {
        Properties properties = readProperties();
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(properties.getProperty("dataSource.jdbcUrl"));
        config.setUsername(properties.getProperty("dataSource.user"));
        config.setPassword(properties.getProperty("dataSource.password"));
        config.setDriverClassName(properties.getProperty("dataSource.className"));
        config.setConnectionTimeout(TimeUnit.SECONDS.toMillis(15));
        config.setMaximumPoolSize(15);
        return new HikariDataSource(config);
    }
}
