/*
 * To change this license header, choose License HeaderesultSet in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctc34.lpas.repository;

import com.ctc34.lpas.connection.Pool;
import com.ctc34.lpas.domain.Ticket;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author Admin
 */
public class TicketRepository extends AbstractRepository<Ticket> {

    public TicketRepository() {
        super.DELETE_SQL = "DELETE FROM tickets WHERE id_ticket=?";
    }

    @Override
    public Optional<Ticket> readById(int id) {
        Ticket ticket = null;
        try (Connection connection = Pool.INSTANSE.getConnection();
                PreparedStatement preparedStatement = createPreparedStatementRead(connection, id);
                ResultSet resultSet = preparedStatement.executeQuery();) {
            while (resultSet.next()) {
                ticket = new Ticket();
                ticket.setId(resultSet.getInt("id_ticket"));
                ticket.setQuestion(resultSet.getString("question"));
                String answer[] = new String[]{
                    resultSet.getString("answer_0"),
                    resultSet.getString("answer_1"),
                    resultSet.getString("answer_2"),
                    resultSet.getString("answer_3")};
                ticket.setAnswers(answer);
                ticket.setUseInt(resultSet.getInt("is_use"));
                ticket.setFkChapter(resultSet.getInt("fk_chapter"));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return Optional.ofNullable(ticket);
    }

    private PreparedStatement createPreparedStatementRead(Connection connection, int id) throws SQLException {//TODO revert back old style as it was
        String SQL_QUERY = "SELECT id_ticket, question, answer_0, answer_1, answer_2, answer_3, is_use, fk_chapter FROM tickets WHERE id_ticket=?";
        PreparedStatement preparedStatement = connection.prepareStatement(SQL_QUERY);
        preparedStatement.setInt(1, id);
        return preparedStatement;
    }

    @Override
    public List<Ticket> readAll() {
        List<Ticket> tickets = new ArrayList<>();
        String SQL_QUERY = "SELECT id_ticket, question, answer_0, answer_1, answer_2, answer_3, is_use, fk_chapter FROM tickets";
        try (Connection connection = Pool.INSTANSE.getConnection();
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(SQL_QUERY);) {
            while (resultSet.next()) {
                Ticket ticket = new Ticket();
                ticket.setId(resultSet.getInt("id_ticket"));
                ticket.setQuestion(resultSet.getString("question"));
                String answer[] = new String[]{
                    resultSet.getString("answer_0"),
                    resultSet.getString("answer_1"),
                    resultSet.getString("answer_2"),
                    resultSet.getString("answer_3")};
                ticket.setAnswers(answer);
                ticket.setUseInt(resultSet.getInt("is_use"));
                ticket.setFkChapter(resultSet.getInt("fk_chapter"));
                tickets.add(ticket);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return tickets;
    }

    @Override
    public int create(Ticket ticket) {
        int createdId = 0;
        String SQL_QUERY = "INSERT INTO tickets (question, answer_0, answer_1, answer_2, answer_3, is_use, fk_chapter) VALUES (?,?,?,?,?,?,?)";
        ResultSet resultSet = null;
        try (Connection connection = Pool.INSTANSE.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SQL_QUERY, Statement.RETURN_GENERATED_KEYS);) {
            preparedStatement.setString(1, ticket.getQuestion());
            preparedStatement.setString(2, ticket.getAnswers()[0]);
            preparedStatement.setString(3, ticket.getAnswers()[1]);
            preparedStatement.setString(4, ticket.getAnswers()[2]);
            preparedStatement.setString(5, ticket.getAnswers()[3]);
            preparedStatement.setInt(6, ticket.getUseInt());
            preparedStatement.setInt(7, ticket.getFkChapter());
            preparedStatement.executeUpdate();
            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                createdId = resultSet.getInt(1);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (null != resultSet) {
                try {
                    resultSet.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return createdId;
    }

    @Override
    public boolean update(Ticket ticket) {
        String SQL_QUERY = "UPDATE tickets SET question=?, answer_0=?, answer_1=?, answer_2=?, answer_3=?, is_use=?, fk_chapter=? WHERE id_ticket=?";
        boolean success = false;
        try (Connection connection = Pool.INSTANSE.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SQL_QUERY);) {
            preparedStatement.setString(1, ticket.getQuestion());
            preparedStatement.setString(2, ticket.getAnswers()[0]);
            preparedStatement.setString(3, ticket.getAnswers()[1]);
            preparedStatement.setString(4, ticket.getAnswers()[2]);
            preparedStatement.setString(5, ticket.getAnswers()[3]);
            preparedStatement.setInt(6, ticket.getUseInt());
            preparedStatement.setInt(7, ticket.getFkChapter());
            preparedStatement.setInt(8, ticket.getId());
            success = (preparedStatement.executeUpdate() == 1);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return success;
    }

}
