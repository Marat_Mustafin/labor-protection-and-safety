package com.ctc34.lpas.repository;

import lombok.Getter;

@Getter
public enum Repository {

    INSTANSE;

    private UserRepository userRepository = new UserRepository();
    private TicketRepository ticketRepository = new TicketRepository();
    private ChapterRepository chapterRepository = new ChapterRepository();

}
