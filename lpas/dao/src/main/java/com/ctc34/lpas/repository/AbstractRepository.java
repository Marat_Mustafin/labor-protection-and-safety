/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctc34.lpas.repository;

import com.ctc34.lpas.connection.Pool;
import com.ctc34.lpas.repository.specification.Specification;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author Admin
 */
public abstract class AbstractRepository<Entity> {

    protected String DELETE_SQL;
    
    public abstract Optional<Entity> readById(int id);

    public List<Entity> read(Specification specification) {
        return specification.search();
    }

    public abstract List<Entity> readAll();

    public abstract int create(Entity entity);

    public abstract boolean update(Entity entity);

    public boolean delete(int id){
        boolean success = false;
        try (Connection connection = Pool.INSTANSE.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(DELETE_SQL);) {
            preparedStatement.setInt(1, id);
            success = (preparedStatement.executeUpdate() == 1);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return success;
    }
}
