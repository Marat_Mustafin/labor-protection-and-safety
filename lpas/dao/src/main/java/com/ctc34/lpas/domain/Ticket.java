/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctc34.lpas.domain;

import lombok.Data;

/**
 *
 * @author me
 */
@Data
public class Ticket extends Entity {

    private String question;
    private String[] answers;
    private boolean isUse;
    private int fkChapter;

    public void setUseInt(int isUse) {
        this.isUse = isUse == 1;
    }

    public int getUseInt() {
        return (isUse) ? 1 : 2;
    }
}
