/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ctc34.lpas.repository.specification;

import java.util.List;

/**
 *
 * @author Admin
 */
public abstract class Specification<Entity> {

    public abstract List<Entity> search();
}
